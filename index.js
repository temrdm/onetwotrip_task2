const N = 2

main()

function main() {
    if (N < 0 || N % 1 !== 0) {
        console.error('N must be a positive integer')
        return 1
    }

    const matrix = makeMatrix(N)
    const list = getConvertedMatrixToList(matrix, N)

    console.log('Matrix:')
    matrix.forEach((line) => console.log(line.join(', ')))

    console.log('\nResult:')
    console.log(list.join(', '))

    function getConvertedMatrixToList(matrix, N) {
        const DIRECTION = {
            LEFT: 'LEFT',
            RIGHT: 'RIGHT',
            TOP: 'TOP',
            BOTTOM: 'BOTTOM',
        }

        if (matrix.length === 1) {
            return [matrix[0][0]]
        }

        let list = []
        let vSize = 1
        let direction = DIRECTION.LEFT
        let x = N
        let y = N

        while (x > 0 || y > 0) {
            list.push(matrix[y][x])

            switch (direction) {
                case DIRECTION.LEFT: {
                    if (x > N - vSize) {
                        x -= 1
                    } else {
                        y += 1
                        direction = DIRECTION.BOTTOM
                    }
                    break
                }

                case DIRECTION.RIGHT: {
                    if (x < N + vSize) {
                        x += 1
                    } else {
                        y -= 1
                        direction = DIRECTION.TOP
                    }
                    break
                }

                case DIRECTION.TOP: {
                    if (y > N - vSize) {
                        y -= 1
                    } else {
                        if (vSize < N) {
                            vSize += 1
                        }
                        x -= 1
                        direction = DIRECTION.LEFT
                    }
                    break
                }

                case DIRECTION.BOTTOM: {
                    if (y < N + vSize) {
                        y += 1
                    } else {
                        x += 1
                        direction = DIRECTION.RIGHT
                    }
                    break
                }
            }
        }

        list.push(matrix[0][0])

        return list
    }

    function makeMatrix(N) {
        const size = N * 2 + 1
        const matrix = []

        for (let i = 0; i < size; i++) {
            matrix.push([])
            for (let j = 0; j < size; j++) {
                matrix[i].push(makeRandomNum())
            }
        }

        return matrix

        function makeRandomNum() {
            return Math.floor(Math.random() * 10)
        }
    }
}
